package com.origenscanner;

import android.os.Bundle;

import com.facebook.react.ReactActivity;
import com.google.firebase.analytics.FirebaseAnalytics;

public class MainActivity extends ReactActivity {
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "OrigenScannerAndroid");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Origen Scanner Android");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "text");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }

    /**
     * Returns the name of the main component registered from JavaScript. This is used to schedule
     * rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "origenscanner";
    }
}
