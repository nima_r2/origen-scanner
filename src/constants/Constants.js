import Config from 'react-native-config';

export const BASE_URL = Config.API_URL;

export const HEADER_HEIGHT = 70;
export const FOOTER_HEIGHT = 70;
