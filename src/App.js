import React from 'react';
import { View, Text, TouchableOpacity, Platform } from 'react-native';
import NfcManager, {NfcEvents} from 'react-native-nfc-manager';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import HomeScreen from './components/HomeScreen';
import { HEADER_HEIGHT } from './constants/Constants';
import ConfirmationScreen from './components/ConfirmationScreen';
import NFCScreen from './components/NFCScreen';

const AppNavigator = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
    },
    Confirmation: {
      screen: ConfirmationScreen,
    },
    NFC: {
      screen: NFCScreen,
    },
  },
  {
    headerLayoutPreset: 'center',
    defaultNavigationOptions: {
      headerStyle: {
        height: HEADER_HEIGHT,
      },
      headerTitleStyle: {
        fontWeight: 'bold',
        fontFamily: 'RobotoCondensed-Light',
        fontSize: 30,
      },
      title: 'ORIGEN',
      headerBackTitle: null,
      headerLeft: null,
    },
  },
);

const AppContainer = createAppContainer(AppNavigator);

class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}

export default App;
