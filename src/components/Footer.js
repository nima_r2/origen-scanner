import React from 'react';
import { Dimensions, Text, View } from 'react-native';
import { FOOTER_HEIGHT } from '../constants/Constants';

class Footer extends React.Component {

  render() {
    const width = Dimensions.get('window').width;

    return (
      <View
        style={{
          width,
          height: FOOTER_HEIGHT,
          position: 'absolute',
          left: 0,
          bottom: 0,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'black',
        }}
      >
        <Text style={{ color: 'white', fontFamily: 'RobotoCondensed-Regular' }}>
          Copyright &copy; 2020 Origen Art Inc.
        </Text>
      </View>
    );
  }
}

export default Footer;
