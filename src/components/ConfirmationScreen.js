import React from 'react';
import { Dimensions, Image, Text, TouchableOpacity, View } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import { HEADER_HEIGHT } from '../constants/Constants';
import Artwork from '../assets/artwork.png';
import Footer from './Footer';

class ConfirmationScreen extends React.Component {
  handleOnPress() {
    const { navigation } = this.props;
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'Home' }),
      ],
    });
    navigation.dispatch(resetAction);
  }

  render() {
    const { navigation } = this.props;
    const artwork = navigation.getParam('artwork');
    const width = Dimensions.get('window').width;
    const height = Dimensions.get('window').height - HEADER_HEIGHT;

    return (
      <View
        style={{
          width,
          height,
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          flexDirection: 'column',
        }}
      >
        <Image
          style={{ width: 60, height: 60, margin: 20, marginTop: 0 }}
          source={Artwork}
        />
        <Text style={{ marginTop: 10, fontFamily: 'RobotoCondensed-Regular', fontSize: 20 }}>
          CONGRATULATIONS!
        </Text>
        <Text
          style={{
            textAlign: 'center',
            margin: 60,
            fontFamily: 'RobotoCondensed-Regular',
            fontSize: 16,
          }}
        >
          {`The artwork ${artwork.artist.name} - ${artwork.title} was successfully added to your Origen Account.`}
        </Text>
        <TouchableOpacity
          style={{
            padding: 10,
            width: width - 70,
            height: 55,
            margin: 25,
            backgroundColor: '#00b6ff',
            alignItems: 'center',
            justifyContent: 'center',
          }}
          onPress={() => this.handleOnPress()}
        >
          <Text
            style={{
              color: 'white',
              fontFamily: 'RobotoCondensed-Regular',
              fontSize: 16,
            }}
          >
            OK
          </Text>
        </TouchableOpacity>
        <Footer />
      </View>
    );
  }
}

export default ConfirmationScreen;
