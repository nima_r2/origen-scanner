import React from 'react';
import NfcManager, { NfcEvents } from 'react-native-nfc-manager';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import {
  Text,
  TouchableOpacity,
  View,
  Dimensions,
  TextInput,
  Image,
  Keyboard,
} from 'react-native';
import Artwork from '../assets/artwork.png';
import { FOOTER_HEIGHT, HEADER_HEIGHT } from '../constants/Constants';
import Footer from './Footer';

const textInputs = [
  {
    key: 'digit1',
    ref: 'digit1',
    next: 'digit2',
    prev: null,
  },
  {
    key: 'digit2',
    ref: 'digit2',
    next: 'digit3',
    prev: 'digit1',
  },
  {
    key: 'digit3',
    ref: 'digit3',
    next: 'digit4',
    prev: 'digit2',
  },
  {
    key: 'digit4',
    ref: 'digit4',
    next: 'digit5',
    prev: 'digit3',
  },
  {
    key: 'digit5',
    ref: 'digit5',
    next: null,
    prev: 'digit4',
  },
];

class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      digit1: null,
      digit2: null,
      digit3: null,
      digit4: null,
      digit5: null,
    };
  }

  renderTextInputs = () => {
    return textInputs.map(input => {
      return (
        <TextInput
          key={input.key}
          ref={input.ref}
          style={{
            width: 55,
            height: 55,
            borderColor: 'black',
            color: 'black',
            borderWidth: 2,
            padding: 0,
            fontFamily: 'RobotoCondensed-Regular',
            fontSize: 36,
          }}
          maxLength={1}
          keyboardType="numeric"
          textAlign="center"
          returnKeyType={input.next ? 'next' : 'done'}
          onSubmitEditing={
            input.next
              ? () => this.refs[input.next].focus()
              : () => Keyboard.dismiss()
          }
          blurOnSubmit={true}
          onChangeText={text => {
            if (parseInt(text) >= 0 || !text) {
              this.setState({
                [input.key]: text,
              });
            }
          }}
          onSelectionChange={e => {
            const {start, end} = e.nativeEvent.selection;
            if (
              start === end &&
              this.state[input.key] &&
              this.refs[input.key].isFocused()
            ) {
              if (input.next) {
                this.refs[input.next].focus();
              } else {
                Keyboard.dismiss();
              }
            }
          }}
          selectTextOnFocus={true}
          onKeyPress={e => {
            if (
              e.nativeEvent.key === 'Backspace' &&
              input.prev &&
              !this.state[input.key]
            ) {
              this.refs[input.prev].focus();
            }
          }}
          value={this.state[input.key]}
        />
      );
    });
  };

  isDisabled = () => {
    const {digit1, digit2, digit3, digit4, digit5} = this.state;
    return !digit1 || !digit2 || !digit3 || !digit4 || !digit5;
  };

  render() {
    const width = Dimensions.get('window').width;
    const height = Dimensions.get('window').height - HEADER_HEIGHT;
    const {digit1, digit2, digit3, digit4, digit5} = this.state;

    return (
      <View
        style={{
          width: width,
          height: height,
          flex: 1,
        }}>
        <KeyboardAwareScrollView
          style={{
            height: height - FOOTER_HEIGHT,
            flexDirection: 'column',
            flex: 1,
          }}>
          <View
            style={{
              width: width,
              height: height - FOOTER_HEIGHT,
              padding: 20,
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              style={{width: 60, height: 60, margin: 20, marginTop: 0}}
              source={Artwork}
            />
            <Text
              style={{
                marginBottom: 20,
                fontFamily: 'RobotoCondensed-Regular',
                fontSize: 20,
              }}>
              ADD NEW ARTWORK
            </Text>
            <Text
              style={{
                margin: 20,
                fontFamily: 'RobotoCondensed-Regular',
                fontSize: 16,
              }}>
              Please enter the 5 digit tagging code below:
            </Text>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                width: width - 70,
                margin: 25,
              }}>
              {this.renderTextInputs()}
            </View>
            <TouchableOpacity
              style={{
                padding: 10,
                width: width - 70,
                height: 55,
                margin: 25,
                backgroundColor: '#00b6ff',
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={() =>
                this.props.navigation.navigate('NFC', {
                  pin: digit1 + digit2 + digit3 + digit4 + digit5,
                })
              }
              disabled={this.isDisabled()}>
              <Text
                style={{
                  color: 'white',
                  fontFamily: 'RobotoCondensed-Regular',
                  fontSize: 16,
                }}>
                NEXT
              </Text>
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
        <Footer/>
      </View>
    );
  }
}

export default HomeScreen;
