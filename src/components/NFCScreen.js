import React from 'react';
import NfcManager, { NfcEvents, NfcTech } from 'react-native-nfc-manager';
import Config from 'react-native-config';
import { Alert, Text, View, Image, Dimensions, Platform } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import ResourcesAPI from '../services/api/ResourcesApi';
import NfcLogo from '../assets/nfc-logo.png';
import Footer from './Footer';
import { HEADER_HEIGHT, FOOTER_HEIGHT } from '../constants/Constants';
import { HeaderBackButton } from 'react-navigation-stack';

class NFCScreen extends React.Component {
  static navigationOptions = ({navigation}) => {
    return {
      headerLeft: headerProps =>
        Platform.OS === 'ios' ? <HeaderBackButton {...headerProps} /> : null,
    };
  };

  componentDidMount() {
    const {navigation} = this.props;

    let a = NfcManager.start();
    let tech =
      Platform.OS === 'ios'
        ? ['mifare', 'felica', 'iso15693', 'IsoDep']
        : ['NfcA', 'NfcB', 'NfcV', 'NfcF', 'IsoDep', 'MifareClassic', 'MifareUltralight'];
    NfcManager.requestTechnology(tech, {
      alertMessage:
        'Please position your phone directly over the tag you seek to scan. The phone should be no farther than 4 inches away from the tag.',
    })
      .then(resp => NfcManager.getTag())
      .then(tag => {
        console.warn(tag.id);
        NfcManager.setAlertMessageIOS('I got your tag!');
        this._cleanUp();
        return ResourcesAPI.updateResource(
          Config.API_TOKEN,
          {
            tagData: tag.id,
          },
          ['artworks', navigation.getParam('pin'), 'tag'],
        );
      })
      .then(response => {
        const resetAction = StackActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({
              routeName: 'Confirmation',
              params: {artwork: response.data},
            }),
          ],
        });
        navigation.dispatch(resetAction);
      })
      .catch(error => {
        Alert.alert('Error', error.message || error, [
          {
            text: 'OK',
            onPress: () => {
              const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({routeName: 'Home'})],
              });
              return navigation.dispatch(resetAction);
            },
          },
        ]);
      });
  }

  _cleanUp = () => {
    NfcManager.cancelTechnologyRequest().catch(() => 0);
  };
  // componentDidMount() {
  //   const { navigation } = this.props;
  //
  //   NfcManager.start();
  //   NfcManager.setEventListener(NfcEvents.DiscoverTag, tag => {
  //     console.warn('tag', tag);
  //
  //     const data = {
  //       tagData: tag.id,
  //     };
  //
  //     // ResourcesAPI.updateResource(Config.API_TOKEN, data, ['artworks', navigation.getParam('pin'), 'tag'])
  //     //   .then(response => navigation.navigate('Confirmation', { artwork: response.data }))
  //     //   .catch(error => {
  //     //     Alert.alert(
  //     //       'Error',
  //     //       error.message || error,
  //     //       [{ text: 'OK', onPress: () => navigation.navigate('Home') }]
  //     //     );
  //     //   });
  //
  //     NfcManager.setAlertMessageIOS('I got your tag!');
  //     NfcManager.unregisterTagEvent().catch(() => 0);
  //   });
  //
  //   this._test();
  // }
  //
  // componentWillUnmount() {
  //   NfcManager.setEventListener(NfcEvents.DiscoverTag, null);
  //   NfcManager.unregisterTagEvent().catch(() => 0);
  // }
  //
  // _cancel = () => {
  //   NfcManager.unregisterTagEvent().catch(() => 0);
  // };
  //
  // _test = async () => {
  //   try {
  //     await NfcManager.registerTagEvent();
  //   } catch (ex) {
  //     console.warn('ex', ex);
  //     NfcManager.unregisterTagEvent().catch(() => 0);
  //   }
  // };

  componentWillUnmount() {
    NfcManager.setEventListener(NfcEvents.DiscoverTag, null);
  }

  render() {
    const height = Dimensions.get('window').height - HEADER_HEIGHT;

    return (
      <View style={{height, flex: 1}}>
        <View
          style={{
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            height: height - FOOTER_HEIGHT,
          }}>
          <Image
            style={{
              width: 240,
              height: 150,
              margin: 20,
              resizeMode: 'stretch',
            }}
            source={NfcLogo}
          />
          <Text
            style={{
              marginVertical: 30,
              marginHorizontal: 80,
              fontFamily: 'RobotoCondensed-Regular',
              textAlign: 'center',
            }}>
            To read the tag correctly, please place your phone close to the tag.
          </Text>
        </View>
        <Footer/>
      </View>
    );
  }
}

export default NFCScreen;
